:title: Taskwarrior Intro
:data-transition-duration: 0
:css: css/style.css
:skip-help: true

This is a source file for the
`Hovercraft! <https://pypi.python.org/pypi/hovercraft/>`_ presentation tool.

You can get the latest version from

https://gitlab.com/b6d/talk-taskwarrior-intro

To render this presentation to HTML:

.. code:: console

    $ mkdir -p slides
    $ hovercraft taskwarrior-intro.rst slides

Then view the file ``slides/index.html`` with your favourite browser and see
how it turned out.


----

Taskwarrior-Intro
=================

`Bernhard Weitzhofer`_

.. _Bernhard Weitzhofer:
    mailto:bernhardATweitzhofer.org

`gitlab.com/b6d/talk-taskwarrior-intro`_

.. _gitlab.com/b6d/talk-taskwarrior-intro:
    https://gitlab.com/b6d/talk-taskwarrior-intro

----

.. image:: ./img/taskwarrior-logo.png
    :align: center

`taskwarrior.org <https://taskwarrior.org>`_

----

Fast keine Dependencies
=======================

* libgnutls
* libuuid

----

Trivialer Build
===============

* CMake
* make
* GCC/Clang

----

Kompiliert überall
==================

----

(ist aber auch überall paketiert)

----

TODO
====

----

Interna
=======

----

Skaliert?
=========

----

Skaliert.
=========

----

Skaliert wirklich?
------------------

----

Advanced editing
================

----

Ein Kommando
============

.. code:: console

    $ task [filter] [command] [args]

----

Prioritäten
===========

----

Farben
======

----

Farbschemas
===========

----

UNDO
====

----

Projekte
========

----

Subprojekte
===========

----

Tags
====

----

Kontexte
========

----

Abhängigkeiten
==============

----

Termine
=======

----

Datumsarithmetik
================

----

Waiting
=======

----

Recurrence
==========

----

Kalender
========

----

Urgency
=======

----

Hooks
=====

----

Skaliert wirklich?
==================

----

Skaliert wirklich.
==================

----

Ökosystem
=========

----

Shell
=====

----

Vit
===

----

Timewarrior
===========

----

APIs
====

----

Server
======

----

Clients
=======

----

FLOSS-Vorzeigeprojekt
=====================

----

Danke!
======

(Fragen/Diskussion?)
