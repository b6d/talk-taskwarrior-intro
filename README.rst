==============================
Slides zu: "Taskwarrior-Intro"
==============================

`Open-Source-Treffen`_ München, 26.05.2017

`gitlab.com/b6d/talk-taskwarrior-intro/`_


Slides anzeigen
===============

Die Folien können mit dem Browser angezeigt werden:

.. code:: console

    $ cd talk-taskwarrior-intro
    $ firefox html/index.html


Slides neu rendern
==================

Erstmal Hovercraft_ in einer Virtuellen Umgebung im Verzeichnis
``my_build_env/`` installieren.  (`Python 3`_ und u.U. noch ein paar OS-Pakete
sollten alles sein, was hierfür benötigt wird)

.. code:: console

    $ cd somewhere
    $ python3 -m venv my_build_env/
    $ source my_build_env/bin/activate
    $ pip install hovercraft

Dann alles nach ``slides/`` rendern

.. code:: console

    $ cd talk-taskwarrior-intro
    $ make

Wenn alles erledigt ist, kann man das Verzeichnis ``my_build_env/`` löschen,
um Hovercraft wieder spurlos zu entfernen.


.. Links:

.. _gitlab.com/b6d/talk-taskwarrior-intro/:
    https://gitlab.com/b6d/talk-taskwarrior-intro
.. _Hovercraft: https://hovercraft.readthedocs.io
.. _Open-Source-Treffen: https://www.opensourcetreffen.de
.. _pyScss: http://pyscss.readthedocs.io
.. _Python 3: https://www.python.org
