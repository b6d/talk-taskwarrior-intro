=======
LICENSE
=======

Everything in this repository not covered by other licenses (see 3rd Party
Content) is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License (http://creativecommons.org/licenses/by-sa/4.0/).

By contributing to this project, you agree to license your contributions under
CC-SA 4.0 as well.


3rd Party Content
=================

Color Scheme "Solarized"
------------------------

Via http://ethanschoonover.com/solarized, MIT License


Font "Droid Sans Mono"
----------------------

Via Google Fonts, Apache License v2.0


Font "Libre Baskerville"
------------------------

Via Google Fonts, Apache License Open Font License


Image "Taskwarrior Logo"
------------------------

Via https://git.taskwarrior.org


