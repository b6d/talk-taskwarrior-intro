all: slides

slides: slides/index.html

slides/index.html: src/taskwarrior-intro.rst
	mkdir -p slides/
	hovercraft src/taskwarrior-intro.rst slides/

clean:
	rm -rf slides/

view:
	x-www-browser slides/index.html

.PHONY: all slides
